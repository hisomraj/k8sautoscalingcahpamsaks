# Kubernetes Autoscaling - Mastering Cluster Autoscaling (CA) and Horizontal Pod Autoscaling (HPA).


# Introduction & Getting started
Autoscaling is the ability to automatically scale a system up or down in response to changes in demand. By automatically scaling the number of nodes or pods in Kubernetes cluster, we can ensure that the applications are always performant, reliable and available.
This guide will provide you with all the information and hands-on experience needed to make the most of Kubernetes Autoscaling.<br>

As someone who has applied many of these strategies/best practices/techniques in real-time projects, I can attest to their effectiveness and the value they will bring to customers. Anyone can follow this guide and refer the documentation(s) mentioned herein to deploy applications successfully & efficiently in real-time projects. Although this blog is centered around CA & HPA, it also contains a curated list of Kubernetes resources that will help to optimize Kubernetes applications and avoid common pitfalls. (I have shared screenshots from these resources with customerrs)<br/>

In this guide, we'll explore the foundations of CA and HPA(powerpoint presentation), then walk through the process of setting up an AKS Cluster, and lastly dive deep into practical demonstrations showcasing how these technologies respond dynamically to load fluctuations. The strategies and techniques can be replicated in AWS & GCP as well. (I have done them for AWS EKS and can confirm). <br>

In Kubernetes, there are two types of autoscaling: Cluster Autoscaling (CA) and Horizontal Pod Autoscaling (HPA).<br>


# Who should use this guide?

This guide is intended to help people at all levels and roles. I have applied many of these techniques/strategies in real-time projects & have witnessed first-hand the positive impact they can have on project outcomes.

1) Kubernetes enthusiasts.
2) AKS users looking to optimize resource utilization, costs & application performance.
3) DevOps teams aiming to implement efficient scaling strategies.
4) Cloud architects & Project/Delivery Manager(s): To design and deploy Kubernetes clusters that are scalable and cost-effective.
5) Anyone working (or will work) in a Kubernetes Scalability Project.
6) People working on Proposals/RFP's that involve Kubernetes: Not just CA and HPA, this blog provides a curated list of Kubernetes resources including the best practices, which will help them to develop more targeted and effective proposals that meet the specific needs of their clients. <br>



# Azure AKS Architecture
Understanding AKS architecture will help anyone to make informed decisions. In AKS the Kubernetes cluster is divided into below two components as can be seen in the image. <br>

1. Control plane: provides the core Kubernetes services and orchestration of application workloads. This is managed by Azure. These provides the core Kubernetes services and orchestration of application workloads in the AKS cluster. <br>
2. Nodes: run client [application workloads on Kubernetes](https://kubernetes.io/docs/concepts/workloads/). These customer-managed nodes run application workloads in the AKS cluster.<br>
3. Pools: A pool is a group of nodes that have an identical configuration.
4. [To learn more you can refer this official training on AKS Cluster Architecture.](https://learn.microsoft.com/en-us/training/paths/aks-cluster-architecture/) <br>

 ![AKS Architecture](/uploads/ecc542a7861086af9c878601dd4c387c/image.png) <br><br>

 The following shows the relationships in a Kubernetes pool between nodes, pods, and containers.
 ![image](/uploads/931df7f7451f4e79340848f1e2be0feb/image.png)<br> <br>

# Azure Kubernetes Service scaling
The following image shows a scaling implementation for Azure Kubernetes Service.<br>
![image](/uploads/755cb1e4ba4aaa916720dc76daf12c57/image.png) <br><br><br>

# Hands-on Demo in Azure Kubernetes Service

<br>

## Open powershell and login with below command. If you have multiple subscriptions then select the correct subcription by second command. Enter the code in https://microsoft.com/devicelogin <br>
```bash
az login --use-device
```
```bash
az account set --subscription <<-subsctption name/id->> (optional if you have multiple subcriptions)
```
## To [view the current/default subscrition](https://learn.microsoft.com/en-us/cli/azure/manage-azure-subscriptions-azure-cli) use the below command (optional).
```bash
az account show --output table
```
## Create resource group `k8scahpa`.
```bash
 az group create --name k8scahpa --location southeastasia
 ```

## [Create an `AKS cluster` with `1 Node`](https://learn.microsoft.com/en-us/cli/azure/aks?view=azure-cli-latest). [We have used the az `aks create command`](https://learn.microsoft.com/en-us/cli/azure/aks?view=azure-cli-latest#az-aks-create). 

Enable the [cluster autoscaler for the new cluster](https://learn.microsoft.com/en-us/azure/aks/cluster-autoscaler#enable-the-cluster-autoscaler-on-a-new-cluster). The `--enable-cluster-autoscaler` parameter enables the cluster autoscaler in the cluster. The `--min-count` and `--max-count` parameters specify the minimum and maximum number of nodes that the cluster autoscaler can scale the cluster to. The cluster autoscaler integrates with the Kubernetes Metrics API to monitor resource utilization and make scaling decisions. It takes into account metrics such as CPU and memory usage to determine when to scale the cluster.
```bash
az aks create --resource-group k8scahpa --name k8scahpa --node-count 1 --vm-set-type VirtualMachineScaleSets --load-balancer-sku standard --enable-cluster-autoscaler --min-count 1 --max-count 2
```
## [The above code will create a `Standard_DS2_v2 machine` - `vCPU=2 RAM=7`](https://learn.microsoft.com/en-us/azure/virtual-machines/dv2-dsv2-series) <br>


## Get the context
```bash
az aks get-credentials -n k8scahpa -g k8scahpa
```

## (optional) Refer below commands for verification of contexts.
````bash
$ kubectl config view
$ kubectl config current-context (output should be aksdemo).
$ kubectl config get contexts
$ kubectl config use-context <<-context name->>
````
## Log on to cluster. And verify that the nuber of nodes and metrics server is enabled. The cluster should have `1 node` and `metrics server` should be running (refer images)
````bash
kubectl get nodes <br>
kubectl get pods -A <br>
````
![image](/uploads/bf74f01d115b2f5c39ed9c124fc8d5a3/image.png) <br>
![image](/uploads/bebf2037921296fb4d29b7b09446d7f8/image.png)<br>

## Deploy the service and the application (`php-apache.yaml`).<br>
````bash
kubectl apply -f php-apache.yaml
````
![image](/uploads/76f4f783ba7ccf7a342ec5482c87fa51/image.png) <br>
## Deploy the HPA (`hpa.yaml`).<br>
````bash
kubectl apply -f hpa.yaml
````
![image](/uploads/44e647fd0e261b44e58333f0e8032d14/image.png) <br>

## Verify the installations by running the below command. The output should be as in image.

````bash
kubectl get pod,deployment,svc,hpa
````
![image](/uploads/e64d18f58cbb37a6102483dab2c8f50b/image.png) <br>

## Run each of the below commands in different powershell windows
````bash
kubectl get hpa php-apache --watch
````
````bash
kubectl get pods --watch
````
````bash
kubectl get nodes
````
## Now, increase the load by running the below command. This will increase the load and the HPA will create additional pods. You can view the `response of CA & HPA` to load fluctuations in the other powershell windows that is opened (refer the screen shots). 
````bash
kubectl run -i --tty load-generator --rm --image=busybox:1.28 --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache; done"
````
![image](/uploads/bff24e407afb60dabef6cafd2998eb4c/image.png) <br>
![image](/uploads/89b4cc496a2e7549d6790c448a523eee/image.png) <br>
![image](/uploads/784bd5eb93a1115c08ae771a9859f4de/image.png)<br>
![Node size increased](/uploads/95bd3fe08ba1218d0daadece3d218b82/image.png) <br>
![image](/uploads/8489a6ed069d4fc83f372442ae1163a0/image.png)<br>

## Stop generating the load by `Ctrl+C`. Then verify the result in other windows (The HPA load should gradually decrease). The `pod count` and `node count` will become 1 again. Pods will come down in few mins but `decrease in node count takes much longer` (15+ minutes)<br>

![image](/uploads/5a209c8dc0c5156fec37ef0d069096fe/image.png) <br>
![image](/uploads/2828fab54ccc42ff6d5749e52e95ac1a/image.png) <br>
![image](/uploads/3ad0abd59efd8207b9055feee1dcd14f/image.png) <br>
![image](/uploads/caa3ade12325a3077b956fbfd7a8a183/image.png) <br>
![image](/uploads/21b211311485bf7e6a07fdd93f69fb68/image.png) <br>
![image](/uploads/56fbad35b185c5e0bbf2594c75ae2aa6/image.png) <br>

#### Delete the resoruce groups
````bash
az group delete --name k8scahpa --yes -y
````
````bash
az group delete --name NetworkWatcherRG --yes -y
````
# Understanding Kubernetes Metrics: Standard and Custom Metrics

In Kubernetes, there are primarily two types of metrics: standard metrics and custom metrics. Understanding the use cases & distinctions is key to optimizing Kubernetes Clusters.

## Standard Metrics (out-of-the-box performance indicators provided by Kubernetes)
Standard metrics, also known as built-in or default metrics, are provided out-of-the-box by Kubernetes. These are metrics that are built into Kubernetes or any Kubernetes component. They are available by default and can be used with CA and HPA without any additional configuration. Standard metrics offer essential information about the cluster, nodes, and pods. Some common standard metrics include CPU utilization, memory usage, network throughput, and pod restarts. These metrics are collected and exposed through the Kubernetes Metrics API, making them readily available for monitoring and analysis.

## Custom metrics
Custom metrics, on the other hand, are specific to an application or workload. These metrics are not included in the default set of metrics provided by Kubernetes but can be defined and collected based on specific business needs. Custom metrics allows to monitor application-specific metrics, business-specific KPIs, or any other data points that are critical to understanding the performance of the application. They can be anything from business-specific performance indicators to highly detailed resource utilization metrics.

To leverage custom metrics effectively: 
1. Instrument the application
2. Configure Metrics Servers
3. Define Scaling Rules

# Best practices for using CA and HPA
Below are some best practices which can be applied in a Kubernetes scalability project. The best practices will vary depending on the specific workload and the requirements of the business. It is important for architects to understand the workload well and to work with the business users to define the desired level of performance and availability. Every workload is unique (so is the business need and trade-offs). What works optimally for one application may not be suitable for another. Hence, a workload-specific approach is essential to ensure the successful implementation of CA and HPA.<br>

1) Monitor and Measure: Before implementing CA and HPA, it is beneficial to establish baseline metrics for the application's resource usage. 
2) [Set Appropriate Resource Requests and Limits](https://cloud.google.com/blog/products/containers-kubernetes/kubernetes-best-practices-resource-requests-and-limits) : Define accurate resource requests and limits in the pod manifests file. Resource requests should reflect the minimum resources a container needs, while resource limits prevent containers from consuming too many resources. This information is essential for HPA to make scaling decisions effectively. <br>
3) Use autoscalers to scale based on metrics that are relevant to the workload -  For example, if customer are scaling a CPU-bound workload, you should use CPU utilization as the target metric.<br>
4) Monitor the performance after the autoscalers have been configured. Adjust the HPA configuration parameters to match application's specific requirements. Experiment and fine-tune these values to achieve optimal scaling behavior.<br>
5) Use autoscalers in conjunction with other Kubernetes features, such as readiness probes and liveness probes : This will help to ensure that the pods that are being scaled are healthy and available.<br>
6) Use autoscalers with caution in production environments : It is important to properly test CA and HPA in a staging environment before deploying them to production. Before deploying in production, conduct load testing and performance testing to understand how the application behaves under different levels of load. <br>
7) Use custom metrics to scale based on metrics that are specific to a workload : The default metrics that are used by CA and HPA, such as CPU utilization and memory utilization, may not be relevant to all workloads. Using custom metrics allows to scale based on metrics that are specific to workload, such as the number of requests per second or the latency of your application. It is important to choose the right metric for the workload. It is a good idea to start with a small number of custom metrics and then add more as needed. The right metric for the workload will depend on the specific requirements of the application. Some experimentation along iteration with business users maybe required.<br>
8) Prevent Flapping : Try to avoid rapid scaling in and out, which can result in "flapping" behavior and increased resource consumption. <br>
9) Rightsize Nodes : Create nodes with appropriate VM sizes and node counts. This prevents underutilization or over-provisioning of nodes.<br>
10) Test Scalability: Before deploying in production, conduct load testing and performance testing.<br>
11) Architects and DevOps teams must have a deep understanding of the workload and collaborate closely with business users to tailor autoscaling strategies to specific application requirements. <br>
12) HPA works best when combined with Cluster Autoscaler. This allows to coordinate scalability of pods with the behavior of nodes in the cluster. When the existing nodes on the cluster are exhausted, HPA cannot scale resources up, so it needs Cluster Autoscaler to help add nodes in the Kubernetes cluster.<br>
13) Go serverless : Serverless services (AWS EKS Fagate) reduces complexity to a large extent. <br>


# Limitations of HPA

Below are some points/limitations to keep in mind. <br>

1) HPA is not compatible with unscalable workloads, such as DaemonSets. Daemonsets work on a “one pod per node” basis, and therefore, they cannot have replicas, which is why HPA is not compatible with Daemonsets.
2) HPA makes scaling decisions based on resource request values at the container level. Therefore, it is essential to have configured the resource request values for all of pods. <br>
3) Due to its inherent limitations, HPA works best when combined with Cluster Autoscaler. <br>
4) HPA and VPA are incompatible. It is not advisable to use both HPA & VPA together for the same set of pods. <br>
5) HPA can only scale pods in a Deployment or StatefulSet. <br>

# Limitations of CA

Below are some points/limitations to keep in mind. <br>

1) Request-based scaling : CA does not make scaling decisions using CPU or memory usage. CA scales a cluster based on the resource requests of the pods running on it, rather than their actual usage. This limitation means that the unused computing resources requested by users will not be detected by CA, resulting in a cluster with waste and low utilization efficiency. Hence right sizing pods is important & HPA is recoommeded along with CA. Scaling kubernetes clusters requires fine-tuning the setting of the autoscalers so that they work in concert. <br>
2) Scaling can take time (node startup time) : Whenever there is a request to scale up the cluster, CA issues a scale-up request to a cloud provider within 30–60 seconds. The actual time the cloud provider takes to create a node can be several minutes or more. This delay means that the application performance may be degraded while waiting for the extended cluster capacity. <br>
3) Some administrator action still required. For example ,Auto Scaling Groups need to be managed independently by the user. Each CSP has some limitations which can be found in their official documentation. <br>
4) CA & HPA can be affected by cloud provider delays. The time it takes to create or delete a pod can be affected by cloud provider delays. This can make HPA and CA less responsive to changes in demand.<br>
5) They can be complex to configure & difficult to troubleshoot : HPA and CA can be complex to configure, especially for workloads with specific requirements.<br>
6) CA cannot scale down nodes that are running pods that are not ready. Pods that are not ready are not yet able to serve traffic. CA cannot scale down nodes that are running pods that are not ready because this could lead to an outage.
7) Resource Fragmentation: Rapid scaling actions can lead to resource fragmentation, where nodes are provisioned and deprovisioned frequently, potentially impacting efficiency.<br>


# Appendix/References

Below are the resources referenced :

1. [HorizontalPodAutoscaler Walkthrough](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/)<br>
2. [Docker Fundamentatls course](https://www.youtube.com/watch?v=gthvzSE4yIY&list=PLTk5ZYSbd9Mg51szw21_75Hs1xUpGObDm)<br>
3. [Complete YAML Course - Beginner to Advanced for DevOps and more](https://www.youtube.com/watch?v=IA90BTozdow)<br>
4. [YAML Tutorial for Beginners: Learn YAML in-depth for DevOps](https://www.youtube.com/watch?v=GOk4IoYhM9U)<br>
5. [Kubernetes for beginners by Technical Guftgu](https://www.youtube.com/watch?v=mYVzuE3daY8&list=PL5yTXsHqphjtp26VEnX_4uE5xZT1WCfMo)<br>
6. [Kubernetes Crash Course: Learn the Basics and Build a Microservice Application](https://www.youtube.com/watch?v=XuSQU5Grv1g)<br>
7. [Sample pratice exercise](https://learn.microsoft.com/en-us/azure/aks/tutorial-kubernetes-scale?source=recommendations&tabs=azure-cli)<br>
8. [Configuration Best Practices](https://kubernetes.io/docs/concepts/configuration/overview/)<br>
9. [Guide to Kubernetes best practices](https://cloud.google.com/blog/products/containers-kubernetes/your-guide-kubernetes-best-practices)<br>
10. [Application scalability on AKS with HorizontalPodAutoscalers](https://learn.microsoft.com/en-us/training/modules/aks-application-autoscaling-native/)<br>



<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>




























































































To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/hisomraj/k8sautoscalingcahpamsaks.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/hisomraj/k8sautoscalingcahpamsaks/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
